import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'contrasena'
})
export class ContrasenaPipe implements PipeTransform {

  transform(value: string, activo: boolean = false): string {
    return (activo) ? '*'.repeat(value.length) : value;
  }

}
