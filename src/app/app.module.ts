import { BrowserModule } from '@angular/platform-browser';
import { NgModule, DEFAULT_CURRENCY_CODE, LOCALE_ID } from '@angular/core';

// para la configuracion de idioma, se debe usar "ng add @angular/localize"
import { registerLocaleData } from '@angular/common';

// importar locales
// import localePy from '@angular/common/locales/es-PY';
// import localePt from '@angular/common/locales/pt';
// import localeEn from '@angular/common/locales/en';
import localeEsPE from '@angular/common/locales/es-PE';
import localeFr from '@angular/common/locales/fr';

// registrar los locales con el nombre que quieras utilizar a la hora de proveer
// registerLocaleData(localePy, 'es');
// registerLocaleData(localePt, 'pt');
// registerLocaleData(localeEn, 'en')
registerLocaleData(localeEsPE, 'es-PE');
registerLocaleData(localeFr);

import { AppComponent } from './app.component';
import { CapitalizadoPipe } from './pipes/capitalizado.pipe';
import { DomseguroPipe } from './pipes/domseguro.pipe';
import { ContrasenaPipe } from './pipes/contrasena.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CapitalizadoPipe,
    DomseguroPipe,
    ContrasenaPipe
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    {provide: DEFAULT_CURRENCY_CODE, useValue: 'PEN'}, // CONFIGURACION POR DEFAULT DE LA MONEDA EN SOLES
    { provide: LOCALE_ID, useValue: 'es-PE' } // idioma
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
